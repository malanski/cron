# Cron React Project  
  
This project is a study on JavaScript and React - Chronometer  
Web Development Couse at Awari School  
<div align="right">
  
### Deploy at: <a href="https://malanski.github.io/cron/">Github pages: Cron</a>

</div> 
  
## Objectives:
- Create React App
- Develop a JavaScript Chronometer  
- Use Fontawesom dependencies 
- Design a CSS style 
- Deploy on gh-pages trough a:`npm run deploy` 

With this project I have learned how to use a javaScript code to count seconds, minutes and hours. as long the page stays open, in order to save background processing.

### Front-end Technologies in this project:
- React  
- JavaScript
- HTML
- CSS
- Node
- Git + Github




